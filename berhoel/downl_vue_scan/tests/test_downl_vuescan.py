"""Testing DownlVueScan."""

# First party library imports.
from berhoel.downl_vue_scan import VueScanInfo

__date__ = "2024/08/07 15:26:51 Berthold Höllmann"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2013 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"


def test_process_row():
    probe = VueScanInfo("file:///old-versions.html")
    assert probe
