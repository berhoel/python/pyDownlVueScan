"""Download current Vuescan version."""

from __future__ import annotations

import functools
import os
from pathlib import Path
import re
import tarfile

from bs4 import BeautifulSoup
from packaging.version import Version
import requests
from rich.console import Console

__date__ = "2024/08/07 15:46:54 Berthold Höllmann"
__author__ = "`Berthold Höllmann <berthold@xn--hllmanns-n4a.de>`__"
__copyright__ = "Copyright © 2013 by Berthold Höllmann"
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"


HEADERS = {
    "version": (
        "Mozilla/5.0 (X11; Linux i686 on x86_64; rv:5.0) " "Gecko/20100101 Firefox/5.0"
    )
}

BASEDIR = Path.home() / "vuescan"
FILE_MASK = re.compile(r"vuescan-x64-(?P<version>\d+\.\d+\.\d\d).tgz$")
ENTRY_RE = re.compile("VueScan.9.[0-9]+.x64")

CONSOLE = Console()


class VueScanInfo:
    """Process html page from Vuescan Homepage."""

    def __init__(self, url: str) -> None:
        """Intitialize class instance."""
        self.url = url
        self.opener = functools.partial(requests.get, headers=HEADERS)
        self.versions: list[Version] = []

    def __call__(self) -> None:
        """Execute code."""
        data = self.opener(self.url)
        doc = BeautifulSoup(data.text, "lxml")
        self.process_data(doc)

    def downl_archive(self, new_ver: Version, i: BeautifulSoup) -> str:
        """Retrieve missing archive."""
        CONSOLE.print(f"downloading latest version {new_ver}")
        ofile = BASEDIR / "downl" / f"vuescan-x64-{new_ver}.tgz"
        if not ofile.is_file():
            package = requests.get(
                f"{'/'.join(self.url.rsplit('/')[:-1])}/{i.get('href')}", timeout=20
            )
            with ofile.open("wb") as out:
                out.write(package.content)
        return f"{ofile}"

    def unpack_archive(self, archive: str, new_ver: Version) -> Path | None:
        """Unpacke installation package."""
        CONSOLE.print(f"unpacking latest version {new_ver}")
        with tarfile.open(archive, "r:gz") as tar:
            tar.extraction_filter = getattr(
                tarfile, "data_filter", (lambda member, _: member)
            )
            tar.extractall(path=f"{BASEDIR}")  # noqa:S202
            for tarinfo in tar:
                if tarinfo.isdir():
                    return BASEDIR / tarinfo.name
        return None

    def process_row(self, row: BeautifulSoup) -> None:
        """Extract information from table row."""
        for i in row.find_all("a"):
            if i.get("href").endswith("tgz"):
                new_ver = Version(i.parent.text.strip())
                downl = BASEDIR / "downl"

                for name in downl.glob("*"):
                    res = FILE_MASK.search(str(name))
                    if res is not None:
                        self.versions.append(Version(res.group("version")))
                self.versions.sort()
                if new_ver in self.versions:
                    CONSOLE.print(f"latest version ({new_ver}) already downloaded")
                    raise SystemExit(0)
                archive = self.downl_archive(new_ver, i)
                origdir = self.unpack_archive(archive, new_ver)
                targetdir_full = BASEDIR / f"vuescan-x64-{new_ver}"
                if not isinstance(origdir, Path):
                    raise TypeError
                origdir.rename(targetdir_full)
                targetdir_short = BASEDIR / "vuescan"
                targetdir_short.unlink(missing_ok=True)
                os.symlink(f"{targetdir_full}", f"{targetdir_short}")

    def process_table(self, table: BeautifulSoup) -> None:
        """Extract information from tables."""
        data = table.find_all("td")
        for entry in data:
            if ENTRY_RE.search(entry.text):
                row = entry.parent
                self.process_row(row)

    def process_data(self, doc: BeautifulSoup) -> None:
        """Generate the csv file."""
        tables = doc.find_all("table")

        for table in tables:
            self.process_table(table)


def main() -> None:
    """Main program."""
    prog = VueScanInfo("https://www.hamrick.com/alternate-versions.html")
    prog()

    raise SystemExit(0)


if __name__ == "__main__":
    main()
